import {TreeListMock} from "./tree-list/tree-list.mock";
import {LkTableMock} from "./lk-table/lk-table.mock";

export const Mock = {
  lk_treeList: TreeListMock,
  lk_table: LkTableMock,
}
