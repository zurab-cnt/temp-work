import { MzMockGenerator } from 'mz-mock';
import {ResponseTableListItemInterface} from "../../../../app/@res/@module/@sub/@service/table-data/@res/@interface/common.interface";
/*
* create custom mock with mz-mock library
* */
const names = [
  'Эрик',
  'Рем',
  'Давид',
  'Марсель',
  'Тигран',
  'Аполлинарий',
  'Леонард',
  'Ангел',
  'Гарри',
  'Альфред',
];

const lastNames = [
  'Челомцев',
  'Покровский',
  'Зюганов',
  'Кораблёв',
  'Белоусов',
  'Витаев',
  'Дятлов',
  'Рытин',
  'Киприянов',
  'Белочкин',
];

const mock = new MzMockGenerator(
  {
    default: names,
    lastnames: lastNames
  }
)

export const GetUsersMock: ResponseTableListItemInterface[] = <any>mock.get(
  {
    age: '__random.number(18, 50)__',
    lastname: '__box(lastnames)__',
    name: '__box(default)__',
    id: '__idx(1)__'
  },
  20
)
