import {ResponseTreeListItemInterface} from "../../../../app/@res/@module/@sub/@service/tree-data/@res/@interface/common.interface";

export const GetTreeListMock: ResponseTreeListItemInterface[] =  [
  {
    name: 'Договоры',
    children: [
      {
        name: 'Входящие',
        children: [
          { name: 'Поставки' },
          { name: 'Прочие', id: 1 },
        ]
      },
      {
        name: 'Исходящие',
        children: [
          { name: 'Поставки' }
        ]
      }
    ]
  },
  {
    name: 'Справочники',
    children: [
      {
        name: 'Справочник 1'
      }, {
        name: 'Справочники 2'
      },
    ]
  },
]
