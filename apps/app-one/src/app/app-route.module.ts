import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {LkComponent} from "./@res/@module/lk/lk.component";

const routesArray: Routes = [
  {
    path: 'lk',
    component: LkComponent
  },
  {
    path: '**',
    redirectTo: '/lk',
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routesArray),
  ],
  exports: [RouterModule],
})
export class AppRouteModule {

}
