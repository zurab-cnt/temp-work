import {NgModule} from "@angular/core";
import {OverlayModule} from "@angular/cdk/overlay";
import {ReactiveFormsModule} from "@angular/forms";
import {FormErrorsForDirective} from "../../@directive/formErrorsFor/formErrorsFor.directive";

@NgModule(
  {
    declarations: [
      FormErrorsForDirective
    ],
    imports: [
      OverlayModule,
      ReactiveFormsModule
    ],
    exports: [
      OverlayModule,
      ReactiveFormsModule,
      FormErrorsForDirective
    ]
  }
)
export class BaseSharedModule {

}
