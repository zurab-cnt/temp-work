export interface FormErrorsForItemInterface {
  /* text error */
  text: string;
  /* error validators */
  falseValidator: string;
  /* */
  touch?: boolean;
}
