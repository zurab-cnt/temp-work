import {
  ChangeDetectorRef,
  Directive,
  Input,
  IterableDiffer,
  IterableDiffers,
  OnChanges,
  SimpleChanges,
  TemplateRef,
  ViewContainerRef
} from "@angular/core";
import {FormErrorsForItemInterface} from "./@res/@interface/common.interface";
import {FormGroup} from "@angular/forms";
import {AutoUnsubscribeClass} from "../../../@res/@class/auto-unsubscribe.class";
import {startWith, takeUntil} from "rxjs/operators";
import {fromEvent, merge, of} from "rxjs";

@Directive(
  {
    selector: '[formErrorFor]'
  }
)
export class FormErrorsForDirective extends AutoUnsubscribeClass implements OnChanges {
  /**
   * */
  @Input() formErrorFor: FormErrorsForItemInterface[];
  /**
   * */
  @Input() formErrorForOf: FormErrorsForItemInterface[];
  /**
   * */
  @Input() formErrorForGroup!: FormGroup;
  /**
   * */
  @Input() formErrorForName!: string;
  /**
   * */
  @Input() formErrorForEl!: HTMLElement;

  /**
   * */
  private iterableDiffer: IterableDiffer<string> | null;

  constructor(
    private viewContainerRef: ViewContainerRef,
    private template: TemplateRef<any>,
    private differs: IterableDiffers,
    private cd: ChangeDetectorRef
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initListerForChange();
  }

  /**
   *
   * */
  private initListerForChange () {
    this.viewContainerRef.clear();

    /* our guard */
    if (!this.formErrorForGroup || !this.formErrorForOf || !this.formErrorForEl) {
      return;
    }

    /* unsubscribe last flows$ */
    this.destroy$.next();

    merge(
      /* for listener values changes */
      this.formErrorForGroup.valueChanges,
      /* for blur changes changes */
      fromEvent(this.formErrorForEl, 'blur')
    ).pipe(
      startWith([null]),
      takeUntil(this.destroy$),
    ).subscribe(
      () => {
        this.safeRender();
      }
    )
  }

  /**
   * */
  private safeRender () {
    this.viewContainerRef.clear();

    const control = this.formErrorForGroup.get(this.formErrorForName),
      errorsKey = Object.keys(control.errors || {}),
      foundError = this.formErrorForOf.find(
        (errorItem) =>  errorsKey.indexOf(errorItem.falseValidator) !== -1 &&
          (errorItem.touch ?? true) === control.touched
      );

    if (foundError) {
      this.viewContainerRef.createEmbeddedView(
        this.template,
        {
          ...(foundError || {}),
          errors: control.errors || {},
          touch: control.touched,
          valid: control.valid,
          $implicit: foundError && foundError.text || null
        }
      );
      this.cd.markForCheck();
    }
  }
}
