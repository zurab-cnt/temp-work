import {Injectable} from "@angular/core";
import {ComponentPortal} from "@angular/cdk/portal";
import {Overlay, OverlayRef} from "@angular/cdk/overlay";
import {OverlayConfig} from "@angular/cdk/overlay/overlay-config";
import {ComponentType} from "@angular/cdk/portal/portal";

@Injectable()
export class ModalOverlay {
  /**
   * */
  public overlayRef: OverlayRef;
  /**
   * */
  public overlay: Overlay;

  constructor(overlay: Overlay) {
    this.overlay = overlay;
  }

  /**
   * open always only one login model
   * */
  public open (
    component: ComponentType<any>,
    options: OverlayConfig = {
      height: '100%',
      width: '100%',
    },
    payload: {[key: string]: any} = {}
  ) {
    this.safeClose();

    this.overlayRef = this.overlay.create(options);

    const instance = this.overlayRef.attach(
      new ComponentPortal(component)
    ).instance,
      instancePayload = {
        ...payload,
        close: () => {
          this.close();
        }
      };

    for(let [key, value] of Object.entries(instancePayload)) {
      instance[key] = value;
    }

  }

  /**
   *
   * */
  public close () {
    this.safeClose();
  }

  /**
   * */
  private safeClose () {
    if (this.overlayRef) {
      this.overlayRef.detach();
    }
  }
}
