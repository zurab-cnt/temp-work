export enum NavigateQueryKeyEnum {
  forMutateTableItem = 'item_id',
  forLoadTable = 'id',
}
