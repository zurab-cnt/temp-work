import {Injectable} from "@angular/core";
import {ResponseTreeListItemInterface} from "../../../@res/@module/@sub/@service/tree-data/@res/@interface/common.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {NavigateQueryKeyEnum} from "./@res/@enum/common.enum";

@Injectable({
  providedIn: "root"
})
export class NavigateService {
  private readonly urlLk = '/lk';

  constructor(
    private router: Router
  ) {
  }

  /**
   *
   * */
  public openTableById(id?: number) {
    this.router.navigate(
      [this.urlLk],
      {
        queryParams: {
          [NavigateQueryKeyEnum.forLoadTable]: id
        }
      }
    )
  }

  /**
   *
   * */
  public openModalWindowForMutateTableItem(id: number, activatedRoute: ActivatedRoute) {
    return this.router.navigate(
      [this.urlLk],
      {
        queryParams: {
          [NavigateQueryKeyEnum.forMutateTableItem]: id
        },
        queryParamsHandling: "merge",
        relativeTo: activatedRoute
      }
    )
  }
}
