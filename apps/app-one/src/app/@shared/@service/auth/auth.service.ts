import {Injectable} from "@angular/core";
import {log} from "util";
import {BehaviorSubject, Observable, of} from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  /**
   * */
  public get login () {
    return window.localStorage.getItem('login') || null;
  };

  /**
   * */
  public set login (value) {
    if (value) {
      window.localStorage.setItem('login', value );
    } else {
      window.localStorage.removeItem('login' );
    }

    this.state$.next(!!value);
  };

  /**
   * */
  public state$: BehaviorSubject<boolean> = new BehaviorSubject(!!this.login);

  /**
   * */
  public signIn(
    login: string,
    pswd: string
  ): Observable<boolean> {
    if (login === 'admin' && pswd === 'admin') {
      this.login = login;
    }
    return of(!!this.login );
  }

  /**
   * */
  public signOut(
  ) {
    this.login = null;
  }
}
