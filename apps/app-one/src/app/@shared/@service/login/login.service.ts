import {Injectable} from "@angular/core";
import {Overlay, OverlayRef} from "@angular/cdk/overlay";
import {ComponentPortal} from "@angular/cdk/portal";
import {LoginComponent} from "../../@view/@dynamic/login/login.component";
import {ModalOverlay} from "../modal/modal.service";

@Injectable({
  providedIn: "root"
})
export class LoginService {
  /** custom class for work with modal */
  private modal: ModalOverlay;

  constructor(
    private overlay: Overlay
  ) {
    this.modal = new ModalOverlay(overlay);
  }

  /**
   * open always only one login modal window
   * */
  public open () {
    this.modal.open(LoginComponent)
  }

  /**
   *
   * */
  public close () {
    this.modal.close();
  }
}
