import {ChangeDetectionStrategy, Component, ElementRef, HostListener, Input, ViewChild} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormErrorsForItemInterface} from "../../../@directive/formErrorsFor/@res/@interface/common.interface";
import {AuthService} from "../../../@service/auth/auth.service";
import {NavigateService} from "../../../@service/navigate/navigate.service";

@Component({
  selector: 'test-workspace-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {
  /**
   * */
  @Input() close!: () => void;

  /**
   * */
  @ViewChild('formMainElement', {read: ElementRef}) formMainElement: ElementRef;

  /**
   *
   * */
  public mainForm = this.fb.group(
    {
      login: [
        '',
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ],
    }
  );

  /**
   * example with errors for custom our directive
   * */
  public errors: FormErrorsForItemInterface[] = [
    {
      falseValidator: 'minlength',
      text: "Недостаточно символов!"
    },
    {
      falseValidator: 'required',
      text: "Обязательно для заполнения!"
    },
    {
      falseValidator: 'email',
      text: "Нужно ввести почту!"
    }
  ];

  /**
   *
   * */
  public showError = false;

  @HostListener(
    'document:click',
    ['$event']
  ) closeAfterClick (event) {
    if (event.target.contains(this.el.nativeElement)) {
      this.close();
    }
  }

  @HostListener(
    'document:keyup.escape',
    ['$event']
  ) closeAfterEsc (event) {
    this.close();
  }


  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private navigateService: NavigateService,
    private el: ElementRef
  ) {

  }

  /**
   * s
   * */
  public submit ($event) {
    this.authService.signIn(
      this.mainForm.get('login').value,
      this.mainForm.get('password').value,
    ).subscribe(
      (result) => {
        this.showError = !result;
        if (result) {
          this.close();
          this.navigateService.openTableById();
        }
      }
    )
  }
}
