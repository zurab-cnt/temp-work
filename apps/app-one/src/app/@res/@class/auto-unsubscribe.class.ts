import {Subject} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable()
export abstract class AutoUnsubscribeClass {
  protected destroy$: Subject<any> = new Subject();

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }
}
