import {NgModule} from "@angular/core";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatTreeModule} from "@angular/material/tree";
import {CommonModule} from "@angular/common";
import {BaseSharedModule} from "../../../@shared/@module/base-shared/base-shared.module";
import {LkComponent} from "./lk.component";
import {TreeDataService} from "../@sub/@service/tree-data/tree-data.service";
import {LkTableComponent} from "./@sub/lk-table/lk-table.component";
import {TableDataService} from "../@sub/@service/table-data/table-data.service";
import {MatTableModule} from "@angular/material/table";
import { LkModalWindowMutateComponent } from './@sub/@dynamic/lk-modal-window-mutate/lk-modal-window-mutate.component';
import {TableItemModalService} from "../@sub/@service/table-item-modal/table-item-modal.service";

@NgModule(
  {
    declarations: [
      LkComponent,
      LkTableComponent,
      LkModalWindowMutateComponent
    ],
    imports: [
      CommonModule,
      BaseSharedModule,
      MatSidenavModule,
      MatToolbarModule,
      MatTreeModule,
      MatTableModule
    ],
    exports: [
      LkComponent
    ],
    providers: [
      TreeDataService,
      TableItemModalService,
      TableDataService
    ]
  }
)
export class LkModule {

}
