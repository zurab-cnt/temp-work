import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LkTableComponent } from './lk-table.component';

describe('LkTableComponent', () => {
  let component: LkTableComponent;
  let fixture: ComponentFixture<LkTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LkTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LkTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
