import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LkModalWindowMutateComponent } from './lk-modal-window-mutate.component';

describe('LkModalWindowMutateComponent', () => {
  let component: LkModalWindowMutateComponent;
  let fixture: ComponentFixture<LkModalWindowMutateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LkModalWindowMutateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LkModalWindowMutateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
