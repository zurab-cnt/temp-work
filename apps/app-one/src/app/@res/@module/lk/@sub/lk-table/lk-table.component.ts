import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {take, takeUntil} from "rxjs/operators";
import {TableDataService} from "../../../@sub/@service/table-data/table-data.service";
import {AutoUnsubscribeClass} from "../../../../@class/auto-unsubscribe.class";
import {ResponseTableListItemInterface} from "../../../@sub/@service/table-data/@res/@interface/common.interface";
import {ActivatedRoute} from "@angular/router";
import {of} from "rxjs";
import {NavigateService} from "../../../../../@shared/@service/navigate/navigate.service";
import {NavigateQueryKeyEnum} from "../../../../../@shared/@service/navigate/@res/@enum/common.enum";
import {MzMockGeneratorRandomService} from "mz-mock/dist/@res/mz-mock/@res/@service/mz-mock-generator/@sub/mz-mock-generator-random/mz-mock-generator-random.service";
import {TableItemModalService} from "../../../@sub/@service/table-item-modal/table-item-modal.service";

@Component({
  selector: 'test-workspace-lk-table',
  templateUrl: './lk-table.component.html',
  styleUrls: ['./lk-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LkTableComponent extends AutoUnsubscribeClass implements OnInit {
  /**
   *
   * */
  public displayedColumns: string[] = ['id', 'name', 'age', 'action'];

  /**
   * */
  public users: ResponseTableListItemInterface[];

  /**
   * */
  public lastIdFromQueryParams: number;

  constructor(
    private tableDataService: TableDataService,
    private cdRef: ChangeDetectorRef,
    private navigateService: NavigateService,
    private activatedRoute: ActivatedRoute,
    private tableItemModalService: TableItemModalService,
  ) {
    super()
  }

  ngOnInit(): void {
    this.initQueryParamsHandler();
  }

  /**
   * */
  private initQueryParamsHandler () {
    this.activatedRoute.queryParams
      .subscribe(
      (params) => {
        this.initUsersList(params[NavigateQueryKeyEnum.forLoadTable]);

        let forLoadTableId;
        if ( (forLoadTableId = params[NavigateQueryKeyEnum.forMutateTableItem]) ) {
          this.tableItemModalService.openModalWindow(forLoadTableId)
        }
      }
    )
  }

  /**
   * */
  private initUsersList (id: number)
  {
    (
      id
        ? this.tableDataService.getListById(id)
        : of(null)
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (result) => {
        this.users = result || [];
        this.cdRef.markForCheck();
      }
    )
  }

  /**
   * for delete item
   * */
  public delete (
    item: ResponseTableListItemInterface
  ) {
    this.tableDataService.delete(item.id)
  }

  /**
   * for navigate to mutate item
   * */
  public mutate (
    item: ResponseTableListItemInterface
  ) {
    this.navigateService.openModalWindowForMutateTableItem(item.id, this.activatedRoute);
  }
}
