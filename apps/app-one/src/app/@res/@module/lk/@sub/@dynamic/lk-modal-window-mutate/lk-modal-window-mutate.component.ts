import {ChangeDetectorRef, Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {FormErrorsForItemInterface} from "../../../../../../@shared/@directive/formErrorsFor/@res/@interface/common.interface";
import {AuthService} from "../../../../../../@shared/@service/auth/auth.service";
import {NavigateService} from "../../../../../../@shared/@service/navigate/navigate.service";
import {ActivatedRoute} from "@angular/router";
import {AutoUnsubscribeClass} from "../../../../../@class/auto-unsubscribe.class";
import {delay, takeUntil} from "rxjs/operators";
import {NavigateQueryKeyEnum} from "../../../../../../@shared/@service/navigate/@res/@enum/common.enum";
import {TableDataService} from "../../../../@sub/@service/table-data/table-data.service";
import {ResponseTableListItemInterface} from "../../../../@sub/@service/table-data/@res/@interface/common.interface";

@Component({
  selector: 'test-workspace-lk-modal-window-mutate',
  templateUrl: './lk-modal-window-mutate.component.html',
  styleUrls: ['./lk-modal-window-mutate.component.scss']
})
export class LkModalWindowMutateComponent extends AutoUnsubscribeClass implements OnInit {
  /**
   * */
  @Input() close!: () => void;

  /**
   * */
  @ViewChild('formMainElement', {read: ElementRef}) formMainElement: ElementRef;

  /**
   *
   * */
  public mainForm = this.fb.group(
    {
      age: [
        '',
        [
          Validators.required,
          Validators.min(18)
        ]
      ],
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ],
      lastname: [
        '',
      ],
    }
  );

  /**
   * example with errors for custom our directive
   * */
  public errors: FormErrorsForItemInterface[] = [
    {
      falseValidator: 'minlength',
      text: "Недостаточно символов!"
    },
    {
      falseValidator: 'required',
      text: "Обязательно для заполнения!"
    },
    {
      falseValidator: 'min',
      text: "Должно быть больше 18!"
    },
    {
      falseValidator: 'email',
      text: "Нужно ввести почту!"
    }
  ];

  /**
   *
   * */
  public showError = false;

  /**
   *
   * */
  public currentId?: number;

  /**
   *
   * */
  public loadedItem?: ResponseTableListItemInterface;

  @HostListener(
    'document:click',
    ['$event']
  ) closeAfterClick (event) {
    if (event.target.contains(this.el.nativeElement)) {
      this.closeWithClearQueryId();
    }
  }

  @HostListener(
    'document:keyup.escape',
    ['$event']
  ) closeAfterEsc (event) {
    this.closeWithClearQueryId();
  }


  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private cdRef: ChangeDetectorRef,
    private activateRoute: ActivatedRoute,
    private tableDataService: TableDataService,
    private navigateService: NavigateService,
    private el: ElementRef
  ) {
    super();
  }

  ngOnInit() {
    this.initQueryParamListener();
  }

  /**
   * */
  public initQueryParamListener () {
    this.activateRoute.queryParams.subscribe(
      (params) => {
        this.currentId = params[NavigateQueryKeyEnum.forMutateTableItem];
        if (this.currentId) {
          this.loadDataById(this.currentId);
        }
      }
    )
  }

  /**
   *
   * */
  public loadDataById (
    id: number
  ) {
    this.tableDataService.getItemById(
      id
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (item) => {
        this.mainForm.patchValue(
          {
            age: item.age,
            name: item.name,
            lastname: item.lastname
          }
        );
      }
    )
  }

  /**
   * */
  public closeWithClearQueryId(): Promise<any> {
    this.close();
    return this.navigateService.openModalWindowForMutateTableItem(null, this.activateRoute)
  }

  /**
   *
   * */
  public submit ($event) {
    if (this.currentId) {
      this.tableDataService.update(
        {
          id: this.currentId || null,
          age: this.mainForm.get('age').value,
          lastname: this.mainForm.get('lastname').value,
          name: this.mainForm.get('name').value
        }
      );
    } else {
      this.tableDataService.create(
        {
          age: this.mainForm.get('age').value,
          lastname: this.mainForm.get('lastname').value,
          name: this.mainForm.get('name').value
        }
      );
    }
    this.closeWithClearQueryId();
  }
}
