import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FlatTreeControl} from "@angular/cdk/tree";
import {MatTreeFlatDataSource, MatTreeFlattener} from "@angular/material/tree";
import {ResponseTreeListItemInterface} from "../@sub/@service/tree-data/@res/@interface/common.interface";
import {LoginService} from "../../../@shared/@service/login/login.service";
import {AutoUnsubscribeClass} from "../../@class/auto-unsubscribe.class";
import {TreeDataService} from "../@sub/@service/tree-data/tree-data.service";
import {takeUntil} from "rxjs/operators";
import {FlatNodeInterface} from "./@res/@intreface/common.interface";
import {AuthService} from "../../../@shared/@service/auth/auth.service";
import {NavigateService} from "../../../@shared/@service/navigate/navigate.service";
import {TableItemModalService} from "../@sub/@service/table-item-modal/table-item-modal.service";

@Component({
  selector: 'test-workspace-lk',
  templateUrl: './lk.component.html',
  styleUrls: ['./lk.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LkComponent extends AutoUnsubscribeClass implements OnInit{
  /**
   * */
  private _transformer = (node: ResponseTreeListItemInterface, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      id: node.id,
      level: level,
    };
  }

  /**
   * */
  public treeControl = new FlatTreeControl<FlatNodeInterface>(
    node => node.level, node => node.expandable);

  /**
   * */
  public treeFlattener = new MatTreeFlattener(
    this._transformer, node => node.level, node => node.expandable, node => node.children);

  /**
   * */
  public dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  /**
   * */
  public hasChild = (_: number, node: FlatNodeInterface) => node.expandable;

  constructor(
    public loginService: LoginService,
    public authService: AuthService,
    public treeDataService: TreeDataService,
    public tableItemModalService: TableItemModalService,
    public navigateService: NavigateService,
  ) {
    super();
  }

  ngOnInit() {
    this.initTreeList();
  }

  /**
   * init data from server
   * */
  private initTreeList () {
    this.treeDataService.getAllRequest().pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (result) => {
        this.dataSource.data = result || [];
      }
    )
  }

  /**
   *
   * */
  public signIn () {
    this.loginService.open();
  }

  /**
   *
   * */
  public signOut () {
    this.authService.signOut();
  }


  /**
   *
   * */
  private openPage(node: ResponseTreeListItemInterface) {
    this.navigateService.openTableById(node.id);
  }

}
