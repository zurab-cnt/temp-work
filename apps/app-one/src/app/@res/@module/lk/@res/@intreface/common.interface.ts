/** Flat node with expandable and level information */
export interface FlatNodeInterface {
  expandable: boolean;
  name: string;
  id?: number;
  level: number;
}
