import {Injectable} from "@angular/core";
import {environment} from "../../../../../../environments/environment";
import {MzMock, MzMockResultTypeEnum} from "mz-mock";
import {BehaviorSubject, Observable, of, Subject} from "rxjs";
import {ResponseTableListItemInterface} from "./@res/@interface/common.interface";
import {Overlay} from "@angular/cdk/overlay";
import {ModalOverlay} from "../../../../../@shared/@service/modal/modal.service";
import {LkModalWindowMutateComponent} from "../../../lk/@sub/@dynamic/lk-modal-window-mutate/lk-modal-window-mutate.component";
import {map, mergeMap, switchMap, tap} from "rxjs/operators";
import {MzMockGeneratorRandomService} from "mz-mock/dist/@res/mz-mock/@res/@service/mz-mock-generator/@sub/mz-mock-generator-random/mz-mock-generator-random.service";

@Injectable()
export class TableDataService {

  /**
   * */
  private usersForState: ResponseTableListItemInterface[] = null;

  /**
   * */
  private stateSubject$: BehaviorSubject<ResponseTableListItemInterface[]> = new BehaviorSubject(null);

  /**
   * */
  public state$ = this.stateSubject$.asObservable();

  /**
   * */
  @MzMock({
    type: MzMockResultTypeEnum.observable,
    path: 'mock.lk_table.getUsers',
    result: environment,
    default: null,
    active: true,
  })
  private getListById_ (id: number): Observable<ResponseTableListItemInterface[]> {
    /* here we add logic with real request */
    return of(null);
  }

  /**
   * */
  public getListById(id: number): Observable<ResponseTableListItemInterface[]> {
    /* here we add logic with real request */
    return this.getListById_(id).pipe(
      switchMap(
        (data) => {
          if (!this.usersForState) {
            this.stateSubject$.next(this.usersForState = data)
          }
          return this.state$
        }
      )
    )
  }


  /**
   * */
  public getItemById(item_id: number): Observable<ResponseTableListItemInterface> {
    /* here we add logic with real request */
    return this.getListById(null).pipe(
      map(
        (items) => items && items.find(
          (item) => item.id === item_id
        )
      )
    );
  }

  /**
   *
   * */
  public delete(id: number) {
    /* here we add logic with real request */
    this.stateSubject$.next(
      this.usersForState = this.usersForState
        ? this.usersForState.filter(
          (item) => item.id !== id
        )
        : this.usersForState
    );
  }

  /**
   *
   * */
  public create(item: ResponseTableListItemInterface) {
    /* here we add logic with real request */
    this.stateSubject$.next(
      this.usersForState = this.usersForState
       ? [
          ...this.usersForState,
          {
            ...item,
            id: MzMockGeneratorRandomService.getRandomNumber(999, 10000)
          }
       ]
       : this.usersForState
    );
  }

  /**
   *
   * */
  public update(item: ResponseTableListItemInterface) {
    /* here we add logic with real request */
    this.stateSubject$.next(
      this.usersForState = this.usersForState
       ?  this.usersForState.map(
          (el) => {
            if (el.id === item.id) {
              el = item
            }
            return el;
          }
       )
       : this.usersForState
    );
  }
}
