export interface ResponseTreeListItemInterface {
  name: string;
  id?: number;
  children?: ResponseTreeListItemInterface[]
}
