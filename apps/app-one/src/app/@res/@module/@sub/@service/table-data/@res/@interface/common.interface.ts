export interface ResponseTableListItemInterface {
  age: number;
  lastname: string;
  name: string;
  id?: number;
}
