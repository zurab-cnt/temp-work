import {ModalOverlay} from "../../../../../@shared/@service/modal/modal.service";
import {Overlay} from "@angular/cdk/overlay";
import {LkModalWindowMutateComponent} from "../../../lk/@sub/@dynamic/lk-modal-window-mutate/lk-modal-window-mutate.component";
import {Injectable} from "@angular/core";

@Injectable()
export class TableItemModalService {

  /** custom class for work with modal */
  private modal: ModalOverlay;

  constructor(
    private overlay: Overlay
  ) {
    this.modal = new ModalOverlay(overlay);
  }

  /**
   * */
  public openModalWindow (id?: number) {
    this.modal.open(
      LkModalWindowMutateComponent,
      {
        height: '100%',
        width: '100%',
      },
      {
        id
      }
    )
  }
}
