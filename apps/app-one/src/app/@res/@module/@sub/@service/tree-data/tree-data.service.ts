import {Injectable} from "@angular/core";
import {environment} from "../../../../../../environments/environment";
import {MzMock, MzMockResultTypeEnum} from "mz-mock";
import {Observable, of} from "rxjs";
import {ResponseTreeListItemInterface} from "./@res/@interface/common.interface";

@Injectable()
export class TreeDataService {

  /**
   * */
  public getById(
    id: string
  ) {

  }

  /**
   * */
  @MzMock({
    type: MzMockResultTypeEnum.observable,
    path: 'mock.lk_treeList.get',
    result: environment,
    default: null,
    active: true,
  })
  public getAllRequest(): Observable<ResponseTreeListItemInterface[]> {
    /* here we add logic with real request */
    return of(null);
  }
}
