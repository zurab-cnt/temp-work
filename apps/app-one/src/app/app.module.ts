import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LoginComponent} from "./@shared/@view/@dynamic/login/login.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LkModule} from "./@res/@module/lk/lk.module";
import {AppRouteModule} from "./app-route.module";
import {BaseSharedModule} from "./@shared/@module/base-shared/base-shared.module";

@NgModule({
  declarations: [AppComponent, LoginComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BaseSharedModule,
    LkModule,
    AppRouteModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
